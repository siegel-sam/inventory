<?php

include('../inc/header.php');

$dbh = $db->prepare('SELECT * FROM count_ledger WHERE item_no = :item_no ORDER BY id DESC');
$dbh->execute(array(':item_no'	=>	$_GET['itemno']));
$results = $dbh->fetchAll(PDO::FETCH_ASSOC);

?>

	<div class="content">
		<div class="fullpage">
			<h1>All Submissions for Item Number <?php echo $_GET['itemno']; ?></h1>
			<table id="singleitem_submissions">
				<thead>
					<th><p>DateTime</p></th>
					<th><p>Username</p></th>
					<th><p>Item</p></th>
					<th><p>Quantity</p></th>
					<th><p>Unit of Measure</p></th>
					<th><p>Location</p></th>
					<th><p>Tag Number</p></th>
				</thead>
				<tbody>
					<?php foreach($results as $result) { ?>
						<tr>
							<td><p><?php echo $result['timestamp']; ?></p></td>
							<td><p><?php echo $result['username']; ?></p></td>
							<td><p><?php echo $result['item_no']; ?></p></td>
							<td><p><?php echo $result['quantity']; ?></p></td>
							<td><p><?php echo $result['uom']; ?></p></td>
							<td><p><?php echo $result['location']; ?></p></td>
							<td><p><?php echo $result['tag_no']; ?></p></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

<?php include('../inc/footer.php');