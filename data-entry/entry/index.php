<?php include('../../inc/header.php'); ?>
	<div class="content">
		<div class="fullpage">
			<table class="left" data-userid="<?php echo $_GET['user_id']; ?>">
				<thead>
					<th>Registering Date</th>
					<th>WDN</th>
					<th>Item No.</th>
					<th>Description</th>
					<th>Lot No.</th>
					<th>Zone Code</th>
					<th>Bin Code</th>
					<th>Quantity</th>
					<th>UOM</th>
				</thead>
				<tbody></tbody>
			</table>
			<table class="right">
				<thead>
					<th>Select</th>
					<th>Complete</th>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
<?php include('../../inc/footer.php'); ?>