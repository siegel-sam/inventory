<?php include('../inc/header.php');

$dbh = $db->prepare('SELECT * FROM nav_users');
$dbh->execute();

$users = $dbh->fetchAll(PDO::FETCH_ASSOC);

?>
	<div class="content">
		<div class="fullpage">
			<h1>NAV Data Entry</h1>
			<h2>Choose your name from the options below:</h2>
			<ul>
				<?php foreach($users as $user) { ?>
					<li><p><a href="./entry?user_id=<?php echo $user['id']; ?>"><?php echo $user['name']; ?></a></p></li>
				<?php } ?>
			</ul>
		</div>
	</div>
<?php include('../inc/footer.php'); ?>