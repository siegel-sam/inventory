<?php

include('db.php');

session_start();

if($_POST['f'] == 'login_submit') {
	$_SESSION['username'] = $_POST['u'];
	echo json_encode(array('success'	=>	true));
}

if($_POST['f'] == 'item_search') {
	$sql_searchTerm = '%' . $_POST['s'] . '%';
	$results = array();
	$query = sqlsrv_prepare($conn, "SELECT * FROM [Siegel Egg Company, Inc_\$Item] WHERE LOWER(Description) LIKE LOWER(?) OR No_ LIKE LOWER(?)", array($sql_searchTerm, $sql_searchTerm) );
	sqlsrv_execute($query);
	while($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) {
		array_push($results, array(
			'No'	=>	$row['No_'],
			'Descr'	=>	$row['Description']
		));
	}

	echo json_encode($results);
}

if($_POST['f'] == 'item_detail') {
	$results = array();
	$uoms = array();
	$query = sqlsrv_prepare($conn, "SELECT Code FROM [Siegel Egg Company, Inc_\$Item Unit of Measure] WHERE [Item No_] = ? AND Code NOT IN ('PALLET', 'LB', 'DZ')", array($_POST['i']));
	sqlsrv_execute($query);
	while($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) {
		array_push($uoms, $row['Code']);
	}

	$base_uom = '';
	$query = sqlsrv_prepare($conn, "SELECT [Base Unit of Measure] FROM [Siegel Egg Company, Inc_\$Item] WHERE No_ = ?", array($_POST['i']));
	sqlsrv_execute($query);
	while($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) {
		$base_uom = $row['Base Unit of Measure'];
	}

	$results['uoms'] = $uoms;
	$results['base_uom'] = $base_uom;

	$dbh = $db->prepare("SELECT * FROM count_ledger WHERE item_no = :item_no ORDER BY id DESC");
	$dbh->execute(array(':item_no'	=>	$_POST['i']));
	$lastcount = $dbh->fetch(PDO::FETCH_ASSOC);

	$results['lastcount'] = $lastcount;

	echo json_encode($results);
}

if($_POST['f'] == 'submit_inventory') {
	
	$dbh = $db->prepare('SELECT tag_no FROM count_ledger WHERE tag_no = :tag_no');
	$dbh->execute(array(':tag_no'	=>	$_POST['t']));
	if($dbh->fetch()) {
		echo json_encode('Error: Tag Already Used');
		exit;
	}

	//select assigned user
	$dbh = $db->prepare('SELECT * FROM nav_users WHERE heartbeat > date_sub(NOW(), INTERVAL 10 second) ORDER BY RAND() LIMIT 1');
	$dbh->execute();
	$user = $dbh->fetch(PDO::FETCH_ASSOC);

	if($user) {
		$user_id = $user['id'];
	} else {
		$user_id = 1;
	}

	$bin = 'C';
	$zone = 'COOLER';

	$query = sqlsrv_prepare($conn, "SELECT TOP 1 [Bin Code], [Zone Code] FROM [Siegel Egg Company, Inc_\$Bin Content] WHERE [Item No_] = ? AND [Bin Type Code] = 'PICKPUT'", array($_POST['i']) );
	sqlsrv_execute($query);
	while($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) {
		$bin = $row['Bin Code'];
		$zone = $row['Zone Code'];
	}

	$dbh = $db->prepare('INSERT INTO count_ledger (username, item_no, item_descr, quantity, uom, location, zone_code, bin_code, tag_no, assigned_to, nav_entry_complete) VALUES (:username, :item_no, :description, :quantity, :uom, :location, :zone_code, :bin_code, :tag_no, :assigned_to, :nav_entry_complete)');

	$dbh->execute(array(
		':username'	=>	$_SESSION['username'],
		':item_no'	=>	$_POST['i'],
		':description'	=>	$_POST['d'],
		':quantity'	=>	$_POST['c'],
		':uom'		=>	$_POST['u'],
		':zone_code'	=>	$zone,
		':bin_code'		=>	$bin,
		':location'	=>	$_POST['l'],
		':tag_no'	=>	$_POST['t'],
		':assigned_to'	=>	$user_id,
		':nav_entry_complete'	=>	0
	));

	echo json_encode(array('success'	=>	true));
}

if($_POST['f'] == 'change_count') {
	$dbh = $db->prepare('INSERT INTO count_ledger (username, item_no, item_descr, quantity, uom, tag_no, comment, assigned_to, nav_entry_complete, location) VALUES (:username, :item_no, :description, :quantity, :uom, :tagno, :comment, :assigned_to, :nav_entry_complete, :location)');
	$dbh->execute(array(
		':username'	=>	$_SESSION['username'],
		':item_no'	=>	$_POST['i'],
		':description'	=>	$_POST['d'],
		':quantity'		=>	$_POST['q'],
		':uom'			=>	$_POST['u'],
		':tagno'		=>	$_POST['t'],
		':comment'		=>	$_POST['c'],
		':location'		=>	'',
		':assigned_to'	=>	1,
		':nav_entry_complete'	=>	0
	));

	echo json_encode(array('success'	=>	true));
}

if($_POST['f'] == 'get_latest') {
	$dbh = $db->prepare('SELECT * FROM count_ledger ORDER BY id DESC LIMIT 10');
	$dbh->execute();
	$results = $dbh->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode($results);
}

if($_POST['f'] == 'get_assigned_nav_counts') {
	$dbh = $db->prepare('SELECT * FROM count_ledger WHERE assigned_to = :user_id AND nav_entry_complete = 0');
	$dbh->execute(array(':user_id'	=>	$_POST['u']));
	$res = $dbh->fetchAll(PDO::FETCH_ASSOC);

	echo json_encode($res);

	$dbh = $db->prepare('UPDATE nav_users SET heartbeat = NOW() WHERE id = :user_id');
	$dbh->execute(array(':user_id'	=>	$_POST['u']));
}

if($_POST['f'] == 'complete_nav_entry') {
	$dbh = $db->prepare('UPDATE count_ledger SET nav_entry_complete = 1 WHERE id = :entry_id');
	$dbh->execute(array(':entry_id'	=>	$_POST['i']));
}

if($_POST['f'] == 'get_tag_data') {

	$dbh = $db->prepare('INSERT INTO spotchecks (username, tag_no) VALUES (:username, :tag_no)');
	$dbh->execute(array(':username'	=>	$_SESSION['username'], ':tag_no'	=>	$_POST['t']));

	$dbh = $db->prepare('SELECT * FROM count_ledger WHERE tag_no = :tag_no ORDER BY timestamp DESC');
	$dbh->execute(array(':tag_no'	=>	$_POST['t']));
	if($res = $dbh->fetch(PDO::FETCH_ASSOC)) {
		echo json_encode($res);
	} else {
		echo json_encode(false);
	}
}