<?php

session_start();

if($_SERVER['REQUEST_URI'] == '/data-entry/') {
	$_SESSION['username'] = 'data-entry';
}

if( !isset($_SESSION['username']) && $_SERVER['REQUEST_URI'] != '/' ) {
	header('Location: /');
}

include('db.php');

$all_items = array();
$query = sqlsrv_prepare($conn, "SELECT * FROM [Siegel Egg Company, Inc_\$Item]");
sqlsrv_execute($query);
while($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) {
	array_push($all_items, array(
		'value'	=>	$row['No_'] . ' ' . $row['Description'],
		'No'	=>	$row['No_'],
		'Descr'	=>	$row['Description']
	));
}

?>
<html>
<head>
	<meta name="viewport" content="width=device-width">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="/main.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<?php if( isset($_SESSION['username']) && ($_SERVER['REQUEST_URI'] == '/inventory/' || $_SERVER['REQUEST_URI'] == '/spot-check/')) { ?>
	<script>
		const all_items = <?php echo json_encode($all_items); ?>;
	</script>
	<?php } ?>

	<!-- <script src="https://cdn.jsdelivr.net/npm/@ericblade/quagga2/dist/quagga.min.js"></script> -->

	<script src="/scripts/main.js"></script>
</head>
<body>