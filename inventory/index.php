<?php

include('../inc/header.php');

?>

	<div id="quaggaCam"></div>
	<div class="content">
		<div class="fullpage">
			<div id="inventory_section">
				<h1>Inventory</h1>
				<input type="text" id="item_lookup" placeholder="Enter Item No. or Description" />
				<button type="button" id="search_btn">Search</button>
				<button type="button" id="blank_btn">Empty Rack</button>
			</div>

			<div id="results_section">
				<h1>Results</h1>
				<table id="res_table">
					<thead>
						<th><p>Item No.</p></th>
						<th><p>Description</p></th>
						<th><p>Take Inventory</p></th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>

			<div id="spotcheck">
				<p><a href="/spot-check">Spot Check</a></p>
			</div>

			<h1>Latest Submissions</h1>
			<table id="latest_submissions">
				<thead>
					<th><p>Username</p></th>
					<th><p>Item</p></th>
					<th><p>Quantity</p></th>
					<th><p>Unit of Measure</p></th>
					<th style="display: none"><p>Location</p></th>
					<th><p>Tag Number</p></th>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="lightbox_bg">
		<div class="centered lightbox">
			<h1><span id="item_no"></span>: <span id="item_descr"></span></h1>
			<div id="lastcount">
				<p>Last Counted By: <span id="lastcount_username"></span></p>
				<p>Last Count Performed: <span id="lastcount_datetime"></span></p>
				<!--<p>Last Count Location: <span id="lastcount_location"></span></p>-->
				<p>Last Count Tag Number: <span id="lastcount_tagno"></span></p>
				<p><a id="count_data_link">Click to view All Counting Data</a></p>
			</div>
			<table>
				<tr>
					<td><p>Count: <input inputmode="decimal" id="count_value" /></p></td>
					<td><p>Unit of Measure: <select id="uom_select"></select></p></td>
				</tr>
				<tr>
					<td colspan="2">
						<p>Tag Number:<br />
							<button id="scan">Scan</button>
							<input id="tag_number" disabled="disabled"></input>
							<textarea id="location" style="display: none"></textarea>
						</p>
					</td>
				</tr>
			</table>
			<p><button type="button" id="submit_inventory">Submit</button></p>
			<p><button type="button" id="cancel_submit_inventory">Cancel</button></p>
		</div>
	</div>

<?php include('../inc/footer.php');