jQuery(function($) {

	let timeoutHandle
	let timeoutHandleNAV
	let subm_arr = []

	let isIOS = false

	function iOS() {
	  return [
	    'iPad Simulator',
	    'iPhone Simulator',
	    'iPod Simulator',
	    'iPad',
	    'iPhone',
	    'iPod'
	  ].includes(navigator.platform)
	  // iPad on iOS 13 detection
	  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
	}

	if (iOS()) {
		isIOS = true
	}

	/* if(!isIOS && $('#spot_scan').length) { */
		$('#spot_tag_number').attr('disabled', false)
		$('#spot_scan').hide()
	/* }

	if(isIOS && $('#spot_scan').length) {
		$('#spot_search').hide()
		$('#spot_tag_number').hide()
	}

	function init_quagga() {
		$('#quaggaCam').show()

		Quagga.init({
			inputStream : {
				name: "Live",
				type: "LiveStream",
				target: document.querySelector('#quaggaCam')
			},
			decoder: {
				readers: ["code_128_reader"]
			}
		}, err => {
				if(err) {
					console.log(err)
					return
				}

				console.log("Initialization finished. Ready to start")
				Quagga.start()
		})
	}

	Quagga.onDetected(res => {
		if($('#spot_search').length) {
			$('#quaggaCam').hide()
			spot_search(res.codeResult.code)
		} else {
			$('#tag_number').val(res.codeResult.code)
			$('#quaggaCam').hide()
		}

		Quagga.stop()
	})
	*/

	function spot_search(tag_no) {
		$.ajax({
			url: '/inc/ajax.php',
			dataType: 'json',
			type: 'post',
			data: {
				f: 'get_tag_data',
				t: tag_no
			},
			success: res => {
				if(res) {
					$('.item_number').html(res.item_no)
					$('#item_description').html(res.item_descr)
					$('#quantity').html(res.quantity)
					$('#new_quantity').val(res.quantity)
					$('#username').html(res.username)
					$('#uom').html(res.uom)
					$('.res_tagno').html(tag_no)
					$('.lightbox_bg').show()
					$('#count_details').show()
					$('#complete').hide()
					$('#edit_count_section').hide()

					$.ajax({
						url: '/inc/ajax.php',
						dataType: 'json',
						type: 'post',
						data: {
							i: res.item_no,
							f: 'item_detail'
						},
						success: item_res => {

							if(res.item_no == 'NA' && res.item_descr == 'EMPTY SLOT') {
								$('#new_quantity').attr('disabled', true)
							}

							$('#uom_select option').remove()

							item_res.uoms.forEach(elem => {
								$('#uom_select').append('<option value="' + elem + '">' + elem + '</option>')
							})
							
							$('#uom_select').val(res.uom)
						}
					})

				} else {
					$('.lightbox_bg').hide()
					alert('Tag Number not found.')
				}
			}
		})
	}

	function get_latest(init_load) {
		if($('#latest_submissions').length) {
			$.ajax({
				url: '/inc/ajax.php',
				dataType: 'json',
				type: 'post',
				data: {
					f: 'get_latest'
				},
				success: res => {
					res.forEach(elem => {
						if(!subm_arr.includes(elem.id)) {
							subm_arr.push(elem.id)
							let item = '';
							for (let i=0; i < all_items.length; i++) {
								if(all_items[i].No === elem.item_no) {
									item = elem.item_no + ': ' + all_items[i].Descr
								}
							}

							let appendage_text = '<tr><td><p>'
									+ elem.username + '</p></td><td><p>'
									+ item + '</p></td><td><p>'
									+ elem.quantity + '</p></td><td><p>'
									+ elem.uom + '</p></td><td><p>'
									//+ elem.location + '</p></td><td><p>'
									+ elem.tag_no + '</p></td></tr>'

							if(init_load) {
								$('#latest_submissions tbody').append(appendage_text)
							} else {
								$('#latest_submissions tbody').prepend(appendage_text)
							}
						}
					})
				}
			})

			timeoutHandle = window.setTimeout(get_latest, 10000);
		}
	}

	get_latest(true)

	function get_assigned_nav_counts(init_load) {
		if($('.left').length) {

			//use for adding date to each journal line
			let d = new Date().toLocaleDateString();
			
			$.ajax({
				url: '/inc/ajax.php',
				dataType: 'json',
				type: 'POST',
				data: {
					f: 'get_assigned_nav_counts',
					u: $('.left').data('userid')
				},
				success: res => {
					res.forEach(elem => {

						if(!subm_arr.includes(elem.id)) {
							subm_arr.push(elem.id)

							let t1_appendage_text = '<tr id="ijrow_'
									+ elem.id +
									'"><td>'
									+ d + '</td><td>&nbsp;</td><td>' + elem.item_no +
									'</td><td>'
									+ elem.item_descr + '</td><td>INV2021</td><td>'
									+ elem.zone_code + '</td><td>'
									+ elem.bin_code + '</td><td>'
									+ elem.quantity + '</td><td>'
									+ elem.uom + '</td></tr>'

							let t2_appendage_text = '<tr><td><p><button class="select" data-id="'
									+ elem.id +
									'">Copy</button></p></td><td><p><button class="complete" data-id="'
									+ elem.id + '">Complete</button></p></td></tr>'

							if(init_load) {
								$('.left tbody').append(t1_appendage_text)
								$('.right tbody').append(t2_appendage_text)
							} else {
								$('.left tbody').prepend(t1_appendage_text)
								$('.right tbody').prepend(t2_appendage_text)
							}
						}
					})
				}
			})

			timeoutHandleNAV = window.setTimeout(get_assigned_nav_counts, 10000);
		}
	}

	get_assigned_nav_counts(true)

	let input = document.getElementById('item_lookup')

	if($('#item_lookup').length) {
		input.addEventListener('keyup', e => {
			if (e.keycode == 13) {
				e.prevetDefault()
				$('#search_btn').click()
			}
		})
	}

	function remove_search_results() {
		$('#results_section').hide();
		$('#res_table tbody tr').remove();
	}

	function show_lightbox(item_no, item_description) {
		$('#item_no').text(item_no)
		$('#item_descr').text(item_description)
		$('.lightbox_bg').show()

		/* if(!isIOS) { */
			$('#tag_number').attr('disabled', false)
			$('#scan').hide()
		/* } */

		if(item_no == 'NA' && item_description == 'EMPTY SLOT') {
			$('#count_value').val(0).attr('disabled', true)
		}

		$.ajax({
			url: '/inc/ajax.php',
			dataType: 'json',
			type: 'post',
			data: {
				i: item_no,
				f: 'item_detail'
			},
			success: res => {
				$('#uom_select option').remove()

				res.uoms.forEach(elem => {
					$('#uom_select').append('<option value="' + elem + '">' + elem + '</option>')
				})

				if(res.lastcount) {
					$('#lastcount_username').text(res.lastcount.username)
					$('#lastcount_datetime').text(res.lastcount.timestamp)
					$('#lastcount_location').text(res.lastcount.location)
					$('#lastcount_tagno').text(res.lastcount.tag_no)
					$('#count_data_link').attr('href', '/count-data?itemno=' + item_no).attr('target', '_blank')
					$('#lastcount').show()
				}
				
				$('#uom_select').val(res.base_uom)
			}
		})
	}

	/* $('#scan').on('click', e => {
		init_quagga()
	})

	$('#spot_scan').on('click', e => {
		init_quagga()
	}) */

	$('#tag_number').on('focus', e => {
		$('#tag_number').val('')
	})

	$('#spot_search').on('click', e => {
		spot_search($('#spot_tag_number').val())
	})

	$('.close_lightbox').on('click', e => {
		$('#new_quantity').attr('disabled', false)
		$('.lightbox_bg').click()
		$('#spot_tag_number').val('')
	})

	$('#edit_count').on('click', e => {
		$('#count_details').hide()
		$('#edit_count_section').show()
	})

	$('#spot_tag_number').on('focus', e => {
		$('#spot_tag_number').val('')
	})

	$('#save_changes').on('click', e => {
		if($('#new_quantity').val() && $('#change_reason').val()) {
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: '/inc/ajax.php',
				data: {
					f: 'change_count',
					i: $('.item_number').html(),
					d: $('#item_description').html(),
					q: $('#new_quantity').val(),
					u: $('#uom_select').val(),
					c: $('#change_reason').val(),
					t: $('.res_tagno').html()
				},
				success: res => {
					$('#change_reason').val('')
					$('#edit_count_section').hide()
					$('#complete').show()
				}
			})
		}
	})

	function hide_lightbox() {
		$('#count_value').attr('disabled', false)
		$('.lightbox_bg').hide()
		$('#lastcount').hide();
		$('#count_value').val('')
		$('#tag_number').val('')
		$('#location').val('')
	}

	$('#login_submit').on('click', () => {
			if( $('#username').val() ) {
			$.ajax({
				url: '/inc/ajax.php',
				dataType: 'json',
				type: 'post',
				data: {
					u: $('#username').val(),
					f: 'login_submit'
				},
				xhrFields: {
					withCredentials: true
				},
				success: res => {
					if(res.success) {
						window.location.replace('/inventory')
					}
				}
			})
		}
	})

	$('#edit_item_btn').on('click', e => {
		$('#edit_item').show()
		$('#edit_item_btn').hide()
	})

	$('#cancel_edit_item').on('click', e => {
		$('#edit_item').hide()
		$('#edit_item_btn').show()
	})

	$('#blank_btn_spot').on('click', e => {
		$('#new_quantity').val(0).attr('disabled', true)
		$('#uom_select option').remove()
		$('.item_number').html('NA')
		$('#item_description').html('EMPTY SLOT')
		$('#edit_item').hide()
		$('#edit_item_btn').show()
	})

	if($('#item_lookup').length) {
		$('#item_lookup').autocomplete({
			minLength: 0,
			maxLength: 25,
			select: (event, ui) => {
				if($('#spot_search').length) {
					$('.item_number').html(ui.item.No)
					$('#item_description').html(ui.item.Descr)

					$.ajax({
						url: '/inc/ajax.php',
						dataType: 'json',
						type: 'post',
						data: {
							i: ui.item.No,
							f: 'item_detail'
						},
						success: item_res => {
							$('#uom_select option').remove()
							$('#new_quantity').attr('disabled', false)

							item_res.uoms.forEach(elem => {
								$('#uom_select').append('<option value="' + elem + '">' + elem + '</option>')
							})
							
							$('#uom_select').val(item_res.base_uom)
						}
					})

					$('#edit_item').hide()
					$('#edit_item_btn').show()
				} else {
					show_lightbox(ui.item.No, ui.item.Descr)
					$('#count_value').focus()
					remove_search_results()
				}
				return false;
			},
			source: (request, response) => {
				let results = $.ui.autocomplete.filter(all_items, request.term);

				response(results.slice(0,25));
			},
		})
		.autocomplete('instance')._renderItem = (ul, item) => {
			return $('<li>')
				.append('<div>' + item.No + ': ' + item.Descr + '</div>')
				.appendTo( ul )
		}
	}

	$('#search_btn').on('click', () => {
		remove_search_results()
		if($('#item_lookup').val()) {
			$.ajax({
				url: '/inc/ajax.php',
				dataType: 'json',
				type: 'post',
				data: {
					s: $('#item_lookup').val(),
					f: 'item_search'
				},
				success: res => {
					res.forEach(elem => {
						$('#res_table tbody').append('<tr><td><p>'
							+ elem.No + '</p></td><td><p>'
							+ elem.Descr + '</p></td><td><p><button class="go_button" data-no="'
							+ elem.No + '" data-description="'
							+ elem.Descr + '">GO</button></p></td></tr>')
					});

					$('#results_section').show()
				}
			})
		}
	});

	$('#cancel_submit_inventory').on('click', e => {
		$('.lightbox_bg').click()
	});

	$(document).on('click', '.go_button', e => {
		show_lightbox($(e.target).data('no'), $(e.target).data('description'))
	})

	$(document).on('click', '#blank_btn', e => {
		show_lightbox('NA', 'EMPTY SLOT')
	})

	$(document).on('click', '.lightbox_bg', e => {
		if($(e.target).hasClass('lightbox_bg')) {
			hide_lightbox()
		}
	})

	$(document).on('click', '#submit_inventory', e => {
		if($('#count_value').val() && $('#tag_number').val()) {
			$.ajax({
				url: '/inc/ajax.php',
				dataType: 'json',
				type: 'post',
				data: {
					f: 'submit_inventory',
					i: $('#item_no').text(),
					c: $('#count_value').val(),
					d: $('#item_descr').text(),
					u: $('#uom_select').val(),
					l: $('#location').val(),
					t: $('#tag_number').val()
				},
				success: res => {
					if(typeof res == 'string' && res.includes('Error:')) {
						alert(res)
					} else {
						window.clearTimeout(timeoutHandle)
						remove_search_results()
						hide_lightbox()
						get_latest()
					}
				}
			})
		}
	})

	$(document).on('click', '.select', e => {

		clearSelection()

		const selection = window.getSelection();
		const range = document.createRange();

		let node = document.getElementById('ijrow_' + $(e.target).data('id'))
		range.selectNodeContents(node)
		selection.addRange(range)

		document.execCommand('copy')
	})

	$(document).on('click', '.complete', e => {

		clearSelection()

		$('#ijrow_' + $(e.target).data('id')).remove()
		$(e.target).parents('tr').remove()

		$.ajax({
			url: '/inc/ajax.php',
			dataType: 'json',
			type: 'POST',
			data: {
				f: 'complete_nav_entry',
				i: $(e.target).data('id')
			}
		})

	})

	function clearSelection() {
		if (window.getSelection) {
			window.getSelection().removeAllRanges()
		} else if (document.selection) {
			document.selection.empty()
		}
	}

})