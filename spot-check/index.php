<?php

include('../inc/header.php');

?>
<div id="quaggaCam"></div>
<div class="content">
	<div class="fullpage">
		<div id="inventory_section">
			<h1>Spot-Check</h1>
			<h2>Enter (or scan) label number</h2>
			<button id="spot_scan">Scan</button>
			<input type="text" id="spot_tag_number" />
			<button id="spot_search">Search</button>
			<p><a href="/inventory">Back to Inventory</a></p>
		</div>
	</div>
</div>

<div class="lightbox_bg">
	<div class="centered lightbox">
		<div id="count_details">
			<h2><strong>Item:</strong> <span class="item_number"></span> - <span id="item_description"></span></h2>
			<p><strong>Tag Number:</strong> <span class="res_tagno"></span></p>
			<p><strong>Quantity:</strong> <span id="quantity"></span></p>
			<p><strong>UOM:</strong> <span id="uom"></span></p>
			<p><strong>Counted by:</strong> <span id="username"></span></p>
			<p><button type="button" id="edit_count">Edit</button></p>
			<p><button type="button" class="close_lightbox">Close/Cancel</button></p>
		</div>
		<div id="edit_count_section">
			<h2><strong>Tag Number:</strong> <span class="res_tagno"></span></h2>
			<p><strong>Item:</strong> <span class="item_number"></span><button id="edit_item_btn">Edit</button></p>
			<div id="edit_item">
				<input type="text" id="item_lookup" placeholder="Enter Item No. or Description" />
				<button type="button" id="blank_btn_spot">Empty Rack</button>
				<button type="button" id="cancel_edit_item">Cancel</button>
			</div>
			<p><strong>Quantity:</strong> <input inputmode="decimal" id="new_quantity" /></p>
			<p><strong>Unit of Measure:</strong> <select id="uom_select"></select></p>
			<p><strong>Reason for change:</strong></p>
			<p><textarea id="change_reason" placeholder="Why are you changing the count data?"></textarea></p>
			<p><button type="button" id="save_changes">Save Changes</button></p>
			<p><button type="button" class="close_lightbox">Close/Cancel</button></p>
		</div>
		<div id="complete">
			<h2>Information Updated</h2>
			<p><button type="button" class="close_lightbox">Close</button></p>
		</div>
	</div>
</div>

<?php

include('../inc/footer.php');